import React from 'react'
import BlockEmployees from './components/block-employees'
import BlockMain from './components/block-main'
import CustomerReviews from './components/customer-reviews'
import DescriptionCompany from './components/description-company'
import ExempleProject from './components/exemple-project'
import FeaturesCompany from './components/features-company'
import Indicator from './components/indicators'
import './style.scss'

const HomePage = () => {
  return (
    <div>
      <BlockMain />
      <FeaturesCompany />
      <DescriptionCompany />
      <Indicator />
      <ExempleProject />
      <CustomerReviews />
      <BlockEmployees />
    </div>
  )
}
export default HomePage
