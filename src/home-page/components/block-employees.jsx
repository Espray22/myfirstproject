import React from 'react'
import ImgPeople1 from '../../img/people/people1.png'
import ImgPeople2 from '../../img/people/people2.png'
import Carousel from 'react-multi-carousel'
import 'react-multi-carousel/lib/styles.css'

const BlockPeople = ({ id, img, fio, value, style }) => {
  const classBlock = id % 2 ? 'container' : 'container mt-5'
  return (
    <div className={style ? style : classBlock}>
      <div className={'row'}>
        <div className={'col-12'}>
          <img src={img} className={'people_block-img'} />
        </div>
        <div className={'col-lg-12 test mt-3'}>{fio}</div>
        <div className={'col-lg-12'}>{value}</div>
      </div>
    </div>
  )
}
const data = [
  { id: 1, img: ImgPeople1, fio: 'Иванов Иван', value: 'Руководитель' },
  { id: 2, img: ImgPeople2, fio: 'Иванов Иван', value: 'Программист' },
  { id: 3, img: ImgPeople1, fio: 'Иванов Иван', value: 'Менеджер' },
  { id: 4, img: ImgPeople2, fio: 'Иванов Иван', value: 'HR' },
  { id: 5, img: ImgPeople1, fio: 'Иванов Иван', value: 'Руководитель' },
  { id: 6, img: ImgPeople2, fio: 'Иванов Иван', value: 'Программист' },
  { id: 7, img: ImgPeople1, fio: 'Иванов Иван', value: 'Менеджер' },
  { id: 8, img: ImgPeople2, fio: 'Иванов Иван', value: 'HR' },
]
const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
}

const BlockEmployees = props => {
  return (
    <>
      <div className={'container d-lg-block d-none'}>
        <div className={'row'}>
          <div className={'col-lg-12 mt-4 mb-2 text-center'}>
            <h1>Наши сотрудники</h1>
          </div>
          <div className={'col-lg-12 text-center'}>
            <h3>Люди которыми мы дорожим!</h3>
          </div>
        </div>
        <div className={'row'}>
          {data.map(user => {
            return (
              <div className={'col-lg-3'}>
                <BlockPeople
                  id={user.id}
                  fio={user.fio}
                  img={user.img}
                  value={user.value}
                />
              </div>
            )
          })}
        </div>
      </div>
      <div className={'container d-lg-none d-block'}>
        <div className={'row'}>
          <div className={'col-12'}>
            <Carousel responsive={responsive}>
              {data.map(user => {
                return (
                  <div className={'col-lg-3'}>
                    <BlockPeople
                      id={user.id}
                      fio={user.fio}
                      img={user.img}
                      value={user.value}
                      style={'people_block-img-mobile'}
                    />
                  </div>
                )
              })}
            </Carousel>
          </div>
        </div>
      </div>
    </>
  )
}

export default BlockEmployees
