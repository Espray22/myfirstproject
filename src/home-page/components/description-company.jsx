import React from 'react'
import IconMob from '../../img/content-moc-1.png'

const DescriptionCompany = () => {
  return (
    <section className="content-section pos-rel">
      <div className="left-pattern d-none d-lg-block"></div>
      <div className="container">
        <div className="row align-items-center">
          <div className="col-lg-5 d-none d-lg-block">
            <img className="content-moc" src={IconMob} alt="" />
          </div>
          <div className="col-lg-7">
            <div className="content-info">
              <h2>
                Мы первая в России студия, <br /> внедрившая гибкую методологию
                Scrum. Зачем оно вам?
              </h2>
              <p>
                Вкратце: вы можете менять ваш проект прямо по ходу разработки. В
                отличие от работы по большим, толстым и нудным ТЗ. Как? Ваш
                проект запускается поэтапно. Сначала разрабатываем самые важные
                для вас функции. Затем менее приоритетные, и так далее. Шаг за
                шагом развивая ваш проект. Приоритеты этапов определяете вы
                сами. Можете что-то менять, добавлять или убирать — прямо в
                процессе работы.
              </p>
              <a className="default-btn" href="about.html">
                Попробовать?<span></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
export default DescriptionCompany
