import React, { Component } from 'react'
import './App.css'
import {} from 'bootstrap-4-react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { withRouter } from 'react-router-dom'
import {} from 'bootstrap-4-react'
import './css/style.scss'
import Header from './header'
import HomePage from './home-page'

class App extends Component {
  render() {
    return (
      <div className="">
        <Router history={this.props.history}>
          {/* <Header /> */}

          <Switch>
            <Route path="/" component={HomePage} />
          </Switch>
          {/* Место для Footer */}
        </Router>
      </div>
    )
  }
}

export default withRouter(App)
